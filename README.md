<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"

SPDX-License-Identifier: Apache-2.0
-->
# Sovereign Workplace coturn Helm Chart

This repository contains a Helm chart for deploying coturn.
It is mainly used by Jitsi.



## Prerequisites

Before you begin, ensure you have met the following requirements:

- Kubernetes 1.21+
- Helm 3.0.0+
- PV provisioner support in the underlying infrastructure


## Documentation

The documentation is placed in the README of the coturn helm chart:

- [coturn](charts/coturn)

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright © 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
