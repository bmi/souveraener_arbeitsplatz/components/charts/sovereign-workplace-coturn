## [1.4.5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-coturn/compare/v1.4.4...v1.4.5) (2023-12-21)


### Bug Fixes

* **docs:** Update siging information ([4baa115](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-coturn/commit/4baa115916a015721833118eef2f1c02381d3d9d))

## [1.4.4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-coturn/compare/v1.4.3...v1.4.4) (2023-12-20)


### Bug Fixes

* **ci:** Move repo to Open CoDE ([f1c69b1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-coturn/commit/f1c69b1a879137469e16dfb6698a9cae45549942))

## [1.4.3](https://gitlab.souvap-univention.de/souvap/tooling/charts/coturn/compare/v1.4.2...v1.4.3) (2023-10-16)


### Bug Fixes

* **coturn:** Move documentation to helm chart ([6cbc3c0](https://gitlab.souvap-univention.de/souvap/tooling/charts/coturn/commit/6cbc3c01f0fe5b31f77469ba9528ac1fa591211f))

## [1.4.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/coturn/compare/v1.4.1...v1.4.2) (2023-10-04)


### Bug Fixes

* **coturn:** Cleanup quotes ([ba7e0fc](https://gitlab.souvap-univention.de/souvap/tooling/charts/coturn/commit/ba7e0fc7f152e2b845514853968d9e7f16c9d1f0))

## [1.4.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/coturn/compare/v1.4.0...v1.4.1) (2023-10-04)


### Bug Fixes

* **coturn:** Use "| quote" wherever possible ([49c97ea](https://gitlab.souvap-univention.de/souvap/tooling/charts/coturn/commit/49c97eab6a3f0259a24269271b64e25ae6858315))

# [1.4.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/coturn/compare/v1.3.2...v1.4.0) (2023-09-25)


### Features

* **coturn:** Improve security settings ([f1e2416](https://gitlab.souvap-univention.de/souvap/tooling/charts/coturn/commit/f1e24162abec155494f780a1217ca748ebdd097c))

## [1.3.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/coturn/compare/v1.3.1...v1.3.2) (2023-09-22)


### Bug Fixes

* Remove NamespaceWatcher from Deployment ([c6af69f](https://gitlab.souvap-univention.de/souvap/tooling/charts/coturn/commit/c6af69f00b54a5a0d6349a15c2c9ed1c7e5ea55a))

## [1.3.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/coturn/compare/v1.3.0...v1.3.1) (2023-09-18)


### Bug Fixes

* Remove NamespaceWatcher from Deployment ([f734cc5](https://gitlab.souvap-univention.de/souvap/tooling/charts/coturn/commit/f734cc5a93fe03c98ec61088f71c9768b5b590a1))

# [1.3.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/coturn/compare/v1.2.0...v1.3.0) (2023-08-07)


### Features

* Update to new chart structure ([b94574d](https://gitlab.souvap-univention.de/souvap/tooling/charts/coturn/commit/b94574d41ddf1beb55a2b200df33f1b2c54744e2))
